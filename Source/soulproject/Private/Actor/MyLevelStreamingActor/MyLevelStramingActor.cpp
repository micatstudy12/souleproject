#include "Actor/MyLevelStreamingActor/MyLevelStramingActor.h"

#include "Actor/GameCharacter/GameCharacter.h"

#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"

AMyLevelStramingActor::AMyLevelStramingActor()
{
	Bounds = CreateDefaultSubobject<USphereComponent>(TEXT("BOUNDS"));
	SetRootComponent(Bounds);


	PrimaryActorTick.bCanEverTick = true;
}

void AMyLevelStramingActor::BeginPlay()
{
	Super::BeginPlay();
	Bounds->OnComponentBeginOverlap.AddDynamic(this, &ThisClass::OnBeginOverlap);
	Bounds->OnComponentEndOverlap.AddDynamic(this, &ThisClass::OnEndOverlap);
	
}

void AMyLevelStramingActor::OnBeginOverlap(
	UPrimitiveComponent* OverlappedComponent, 
	AActor* OtherActor, 
	UPrimitiveComponent* OtherComp, 
	int32 OtherBodyIndex, 
	bool bFromSweep, 
	const FHitResult& SweepResult)
{
	// 겹친 액터가 GameCharacter 형식이라면

	
	if (OtherActor->IsA<AGameCharacter>())
	{
		UE_LOG(LogTemp, Warning, TEXT("OnBeginOverlap"));
		FLatentActionInfo info;
		UGameplayStatics::LoadStreamLevel(this, LoadLevelName, true, true, info);
	}

}

void AMyLevelStramingActor::OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor->IsA<AGameCharacter>())
	{
		UE_LOG(LogTemp, Warning, TEXT("OnEndOverlap"));
		FLatentActionInfo info;
		UGameplayStatics::UnloadStreamLevel(this, LoadLevelName, info, true);
	}
}

