#pragma once

#include "CoreMinimal.h"

namespace ECharacterTeam
{
	enum Type : uint8
	{
		Player = 0b0001,
		Enemy = 0b0010,
	};
};
